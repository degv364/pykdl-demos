# Prelude
from numpy.random import random
from numpy import pi, array
from arcos_pykdl import geometry as geo
from arcos_pykdl import chain
from arcos_pykdl import forward_kinematics
from arcos_pykdl import forward_diff_kinematics
from arcos_pykdl import inverse_diff_kinematics
import read_chain

Frame = geo.Frame
Twist = geo.Twist
add_delta = geo.add_delta
diff = geo.diff

Chain = chain.Chain
ForwardKinematicsSolver = forward_kinematics.ForwardKinematicsSolver
ForwardDiffKinematicsSolver = (
    forward_diff_kinematics.ForwardDiffKinematicsSolver)
InverseDiffKinematicsSolver = (
    inverse_diff_kinematics.InverseDiffKinematicsSolver)


# Demo
init = Frame.from_translation_euler(1, 2, 3, 0.1*pi, 0.4*pi, 0.3*pi)
end = Frame.from_translation_euler(2, 7, 0, 0.5*pi, 0.1*pi, 0.2*pi)
twist = diff(end, init, 0.1)
result = add_delta(init, twist, 0.1)

# End and result are very similar

end.data()
twist.data()
result.data()

# Load a kinematic chain from a configuration file
kuka_segments = read_chain.segments_from_file("kinematics_conf.json", "kuka")

chain = Chain()
for segment in kuka_segments:
    chain.add_segment(segment)

chain.get_num_joints()

# Create all available solvers
pose_solver = ForwardKinematicsSolver(chain)
vel_solver = ForwardDiffKinematicsSolver(chain)
inv_solver = InverseDiffKinematicsSolver(chain)

# Create random input data
angles = list(random(7)*pi)
qdots = list(random(7)*pi)
twist = Twist(list(random(6)))

# Use each solver

# Using array for better presentation
array(pose_solver.solve(angles).data())

vel_solver.solve(angles, qdots).data()

inv_solver.solve(twist, angles)
