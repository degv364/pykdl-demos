# -*- coding: utf-8 -*-
#
# Copyright (c) 2017-2018 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from numpy import random
from arcos_pykdl import geometry as geo


# Create an identity frame
iden = geo.Frame.identity()
# Create a null twist
no_move = geo.Twist([0, 0, 0, 0, 0, 0])
# Check that we get the identity
assert(iden == geo.add_delta(iden, no_move, random.random()))
assert(no_move == geo.diff(iden, iden, random.random()))
