# -*- coding: utf-8 -*-
# Copyright (c) 2016 Universidad de Costa Rica,
# Autonomous Robots and Cognitive Systems Laboratory
# Authors: Daniel García Vaglio <degv364@gmail.com>,
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Standard modules
import threading
import time
import json
from functools import reduce
from operator import add

# My package modules
from arcos_pykdl import geometry as geo
from arcos_pykdl import chain
from arcos_pykdl import forward_kinematics as fk
from arcos_pykdl import forward_diff_kinematics as fdk
from arcos_pykdl import inverse_diff_kinematics as idk

# Internal modules
from read_chain import segments_from_conf
from utilities import RoboviewerObjects


def flatten(array):
    # Takes a 2d array and flattens it into a list
    reduce(add, array.data(), [])


class InteractiveObject():
    def __init__(self, vis_id, parent_vis,
                 name="frame",
                 scale=[1.0, 1.0, 1.0],
                 color=[0.5, 0.5, 0.5],
                 pose=geo.Frame.identity()):
        self.view_objects = parent_vis
        self.vis_id = vis_id
        self.name = name
        self.update_scale(scale)
        self.update_color(color)
        self.update_pose(pose)
        self.update_twist([0]*6)

    def update_pose(self, new_pose=geo.Frame.identity(),
                    roll=0, pitch=0, yaw=0, x=0, y=0, z=0):
        # TODO: use RPY, xyz
        self.pose = new_pose
        self.view_objects.send_prop(
            self.vis_id, "pose_offset", flatten(self.pose))
        return self

    def update_twist(self, new_twist):
        self.twist = new_twist
        return self

    def update_scale(self, new_scale):
        self.scale = new_scale
        self.view_objects.send_prop(
            self.vis_id, "scale", self.scale)
        return self

    def update_color(self, new_color):
        self.color = new_color
        self.view_objects.send_prop(
            self.vis_id, "color", self.color
        )
        return self


class InteractiveChain():
    def __init__(self, vis_id, parent_vis, conf):
        self.view_objects = parent_vis
        self.vis_id = vis_id
        self.conf = conf
        segments = segments_from_conf(conf)
        self.chain = chain.Chain()
        if len(segments) != 0:
            for segment in segments:
                self.chain.add_segment(segment)
        self.num_joints = self.chain.get_num_joints()
        self.angles = [0 for i in range(self.num_joints)]
        self.qdots = [0 for i in range(self.num_joints)]
        self.forward_pos_solver = fk.ForwardKinematicsSolver(self.chain)
        self.forward_vel_solver = fdk.ForwardDiffKinematicsSolver(self.chain)
        self.inverse_vel_solver = idk.InverseDiffKinematicsSolver(self.chain)
        self.view_objects.send_conf(self.vis_id, json.dumps(self.conf))
        self.view_objects.send_prop(self.vis_id, "timeout", [-1])

    def pose_from_angles(self, angles):
        """
        Returns the pose of each joint, given its angular state

        :param angles: list of angles [float]
        :returns: list of poses as np.arrays
        """
        if len(angles) != self.num_joints:
            # Should I send all the ones I can? or just fail?
            raise NotImplementedError("Not the correct amount of angles")
        self.angles = angles
        result = self.forward_pos_solver(self.angles)
        return result.data()

    def angle_from_pose(self, end_pose):
        raise NotImplementedError

    def twist_from_qdots(self, qdots):
        """
        Returns the twist of each joint, given its angle and angular vel

        :param qdots: list of angular velocities [float]
        :returns: list of twists as np.arrays
        """
        if len(qdots) != self.num_joints:
            # Should I send all the ones I can? or just fail?
            raise NotImplementedError("Not the correct amount of angles")
        self.qdots = qdots
        result = self.forward_vel_solver.solve(self.angles, self.qdots)
        return result.data()

    def qdots_from_twist(self, twist):
        """
        Returns the joint vels, given end effector twist

        :param twist: np.array six-dimensional velocity
        :returns: list of qdtos [float]
        """
        self.qdots = self.inverse_vel_solver.solve(self.angles, twist)
        return self.qdots

    def update_state(self, dt):
        for index in range(self.num_joints):
            self.angles[index] += self.qdots[index] * dt
        self.view_objects.send_prop(self.vis_id, "angles", self.angles)


class InteractiveHandler():
    def __init__(self, frequency=15.0):
        remote_port = "/0/lwr/roboviewer"
        self.view_objects = RoboviewerObjects(
            "/demos", remote_port, counter=10000)
        self.objects = []
        self.kdl_chains = []
        self.dt = 1.0/frequency
        self.looping = True  # This can be changed from shell async
        self.started = False
        self.updater_thread = threading.Thread(target=self._updater)
        self.start()

    def create_object(
            self,
            object_name="frame",
            scale=[1.0, 1.0, 1.0],
            color=[0.5, 0.5, 0.5],
            pose=geo.Frame.identity()):

        vis_id = self.view_objects.create_object(object_name)
        self.view_objects.send_prop(vis_id, "timeout", [-1])
        self.objects.append(
            InteractiveObject(vis_id, self.view_objects,
                              object_name, scale, color, pose))
        return self.objects[-1]

    def create_chain(self, conf):
        vis_id = self.view_objects.create_object("articulated")
        self.kdl_chains.append(
            InteractiveChain(vis_id, self.view_objects, conf)
        )
        return self.kdl_chains[-1]

    def stop(self):
        self.looping = False
        self.started = False

    def start(self):
        self.updater_thread.start()
        self.started = True

    def _updater(self):
        while self.looping:
            init = time.time()
            if len(self.objects) != 0:
                for obj in self.objects:
                    obj.update_pose(
                        geo.add_delta(obj.pose, obj.twist, self.dt))
            if len(self.kdl_chains) != 0:
                for kdl_chain in self.kdl_chains:
                    kdl_chain.update_state(self.dt)
            end = time.time()
            if end - init > self.dt:
                raise RuntimeWarning("Slow Cycle!")
            else:
                time.sleep(self.dt - end + init)

    def core(self):
        raise DeprecationWarning("The thread starts automatically")
