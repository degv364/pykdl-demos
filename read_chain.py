# -*- coding: utf-8 -*-
# Copyright (c) 2016 Universidad de Costa Rica,
# Autonomous Robots and Cognitive Systems Laboratory
# Authors: Daniel García Vaglio <degv364@gmail.com>,
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from arcos_pykdl import joint as kdljoint
from arcos_pykdl import segment as kdlsegment
from arcos_pykdl import geometry as geo

import json


def _joint_type_from_name(name):
    type_map = {
        "NoJoint": kdljoint.JointType(0),
        "TransX": kdljoint.JointType(1),
        "TransY": kdljoint.JointType(2),
        "TransZ": kdljoint.JointType(3),
        "RotX": kdljoint.JointType(4),
        "RotY": kdljoint.JointType(5),
        "RotZ": kdljoint.JointType(6),
    }
    if name in type_map:
        return type_map[name]
    else:
        # Should I return a default Joint Type?
        raise NotImplementedError("Not a valid name")


def _frame_from_conf(params):
    x, y, z, roll, pitch, yaw = params
    return geo.Frame.from_translation_euler(x, y, z, roll, pitch, yaw)


def segments_from_string(in_string):
    return segments_from_conf(json.loads(in_string))


def segments_from_conf(description_list):
    segments = []
    for item in description_list:
        joint_conf = item["Joint"]
        joint_type = _joint_type_from_name(joint_conf["JointType"])
        joint_scale = joint_conf["Scale"]
        joint = kdljoint.Joint(joint_type)
        joint.set_scale(joint_scale)

        frame_conf = item["Frame"]
        frame = _frame_from_conf(frame_conf)
        segments.append(kdlsegment.Segment(
            joint, frame
        ))

    return segments


def segments_from_file(filename, robot_name):
    with open(filename, "r") as conf_file:
        conf = json.load(conf_file)
        robot_conf = conf[robot_name]

    return segments_from_conf(robot_conf)
